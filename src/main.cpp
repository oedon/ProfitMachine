#include <Arduino.h>
#include<RTClib.h>
#include <SPI.h>
#include <ESP32Servo.h>

// INPUT
#define DA1 23
#define DA2 16
#define DA3 14

#define DB1 19
#define DB2 18
#define DB3 17

#define BTN_A 25
#define BTN_B 26

int LED_BUILTIN = 2;


// RTC
RTC_DS3231 rtc; 
// char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

// Servo
Servo myservo;  // create servo object to control a servo
int pos = 0;    // variable to store the servo position
// Recommended PWM GPIO pins on the ESP32 include 2,4,12-19,21-23,25-27,32-33 
int servoPin = 13;

// VARS
bool runA = false;
bool lockA = false;
bool runB = false;
bool lockB = false;

void setup () {
  Serial.begin(9600);

	// Allow allocation of all timers
	ESP32PWM::allocateTimer(0);
	ESP32PWM::allocateTimer(1);
	ESP32PWM::allocateTimer(2);
	ESP32PWM::allocateTimer(3);
	myservo.setPeriodHertz(50);    // standard 50 hz servo
	myservo.attach(servoPin, 500, 2400); // attaches the servo on pin 18 to the servo object
	// using default min/max of 1000us and 2000us
	// different servos may require different min/max settings
	// for an accurate 0 to 180 sweep


#ifndef ESP8266
  while (!Serial); // wait for serial port to connect. Needed for native USB
#endif
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    abort();
  }
    Serial.println("Setting the time...");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
     rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));

  // PIN MODE
  pinMode(DA1, INPUT_PULLDOWN);
  pinMode(DA2, INPUT_PULLDOWN);
  pinMode(DA3, INPUT_PULLDOWN);

  pinMode(DB1, INPUT_PULLDOWN);
  pinMode(DB2, INPUT_PULLDOWN);
  pinMode(DB3, INPUT_PULLDOWN);

  pinMode(BTN_A, INPUT);
  pinMode(BTN_B, INPUT);

  pinMode (LED_BUILTIN, OUTPUT);
  }

void servoRun() {
	for (pos = 0; pos <= 180; pos += 3) { // goes from 0 degrees to 180 degrees
		// in steps of 1 degree
		myservo.write(pos);    // tell servo to go to position in variable 'pos'
		delay(15);             // waits 15ms for the servo to reach the position
	}
	for (pos = 180; pos >= 0; pos -= 3) { // goes from 180 degrees to 0 degrees
		myservo.write(pos);    // tell servo to go to position in variable 'pos'
		delay(15);             // waits 15ms for the servo to reach the position
	}
}

void loop() {


  DateTime now = rtc.now();
    // Serial.print(now.year(), DEC);
    // Serial.print(now.month(), DEC);
    // Serial.print(now.day(), DEC);
    // Serial.print(daysOfTheWeek[now.dayOfTheWeek()]);
    // Serial.print(now.hour(), DEC);
    // Serial.print(now.minute(), DEC);
    // Serial.print(now.second(), DEC);

    int day = now.dayOfTheWeek();
    int min = now.minute();
    int sec = now.second();

    int da1_read = digitalRead(DA1);
    int da2_read = digitalRead(DA2);
    int da3_read = digitalRead(DA3);
    
    int db1_read = digitalRead(DB1);
    int db2_read = digitalRead(DB2);
    int db3_read = digitalRead(DB3);

    int btna = digitalRead(BTN_A);
    int btnb = digitalRead(BTN_B);

    Serial.print("DAY: ");
    Serial.println(day);
    Serial.print("RUNA: ");
    Serial.println(runA);
    Serial.print("BTNA: ");
    Serial.println(btna);
    Serial.print("BTNB: ");
    Serial.println(btnb);

    // Time Lock
    if (min % 5 == 0 && sec == 0)
    {
      lockA = false;
      digitalWrite(LED_BUILTIN, LOW);
    }

    // Run Lock
    if (min % 2 == 0)
    {
      runA = false;
    }

    // BTN_A TEST
    if (btna == 1 && da1_read == 0 && da2_read == 0 && da3_read == 0)
    {
      servoRun();
    }
    // BTN_B TEST
    if (btnb == 1 && db1_read == 0 && db2_read == 0 && db3_read == 0)
    {
      servoRun();
    }

    // BTN_A LOGIC
    if (btna == 1 && lockA == false && runA == false)
    {
      // 1x
      if (da1_read == 1)
      {
        servoRun();
      }
      // 2x
      if (da2_read == 1)
      {
        for (int i = 0; i <= 1; i++)
        {
          servoRun();
        }
      }
      // 3x
      if (da3_read == 1)
      {
        for (int i = 0; i <= 3; i++)
        {
          servoRun();
        }
      }

      lockA = true;
      runA = true;
      digitalWrite(LED_BUILTIN, HIGH);
    }



    delay(100);
}
